package world.reader.booklist.downloader;

import org.junit.Test;

import world.reader.booklist.data.Sha256Hash;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

/**
 * Created by plopez on 21/03/17.
 */
public class Sha256HashTest {

    @Test
    public void generateHash(){
          String url= "/categories/eng";
          String timestamp = "0";
          String apiToken = "nVsCYXf7C929mQjY5uA9cfBP";
          String actual = Sha256Hash.hash(url + timestamp + apiToken);
          assertThat(actual, is("9268e388e1376d88b45d7061fa117e4cbf570e7856e5600fad93d590baffea5b"));
      }

}