package world.reader.booklist.data.events;

/**
 * Created by plopez on 23/03/17.
 */

public class OnErrorDownloading {

    private Throwable throwable;

    public OnErrorDownloading(Throwable throwable) {
        this.throwable = throwable;
    }

    public Throwable getThrowable() {
        return throwable;
    }
}
