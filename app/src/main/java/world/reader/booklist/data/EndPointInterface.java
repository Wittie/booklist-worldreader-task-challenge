package world.reader.booklist.data;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.Query;
import world.reader.booklist.model.Book;
import world.reader.booklist.model.Category;

/**
 * Created by plopez on 22/03/17.
 */

public interface EndPointInterface {

    String SERVER = "https://qa.api.worldreader.futuhosting.com";

    String ENDPOINT_CATEGORIES = "/categories/eng";

    String ENDPOINT_BOOK = "/books";

    String HEADER_API_TOKEN_VALUE = "nVsCYXf7C929mQjY5uA9cfBP";

    String HEADER_CONTENT_TYPE_KEY = "Content-Type";
    String HEADER_CONTENT_TYPE_VALUE = "application/json";
    String HEADER_CONTENT_TYPE = HEADER_CONTENT_TYPE_KEY + ": " + HEADER_CONTENT_TYPE_VALUE;

    String HEADER_ACCEPT_LANGUAGE_KEY = "Accept-Language";
    String HEADER_ACCEPT_LANGUAGE_VALUE = "es";
    String HEADER_ACCEPT_LANGUAGE = HEADER_ACCEPT_LANGUAGE_KEY + ": " + HEADER_ACCEPT_LANGUAGE_VALUE;

    String HEADER_CLIENT_KEY = "X-Worldreader-Client";
    String HEADER_CLIENT_VALUE = "org.worldreader.wrms.android/1.0.0.0";
    String HEADER_CLIENT = HEADER_CLIENT_KEY + ": " + HEADER_CLIENT_VALUE;

    String HEADER_TIMESTAMP_KEY = "X-Worldreader-Timestamp";

    String HEADER_HASH_KEY = "X-Worldreader-Hash";


    @GET(ENDPOINT_CATEGORIES)
    @Headers({HEADER_CONTENT_TYPE, HEADER_ACCEPT_LANGUAGE, HEADER_CLIENT})
    Call<List<Category>> getCategories(@Header(HEADER_TIMESTAMP_KEY) String timestamp,
                                       @Header(HEADER_HASH_KEY) String hash);

    @GET(ENDPOINT_BOOK)
    @Headers({HEADER_CONTENT_TYPE, HEADER_ACCEPT_LANGUAGE, HEADER_CLIENT})
    Call<List<Book>> getBookOfCategory(@Query("category") String category, @Query("sort") String dateDesc,
                                       @Header(HEADER_TIMESTAMP_KEY) String timestamp,
                                       @Header(HEADER_HASH_KEY) String hash);

}