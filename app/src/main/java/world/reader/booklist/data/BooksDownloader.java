package world.reader.booklist.data;

import android.util.Log;

import org.greenrobot.eventbus.EventBus;

import java.util.Date;
import java.util.List;

import javax.inject.Inject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import world.reader.booklist.BookListApplication;
import world.reader.booklist.data.events.OnErrorDownloading;
import world.reader.booklist.data.events.OnErrorSavingOnDataBase;
import world.reader.booklist.data.events.OnRefreshedBooks;
import world.reader.booklist.model.Book;
import world.reader.booklist.view.DataBaseRepository;

import static world.reader.booklist.data.EndPointInterface.ENDPOINT_BOOK;
import static world.reader.booklist.data.EndPointInterface.HEADER_API_TOKEN_VALUE;

/**
 * Created by plopez on 23/03/17.
 */

public class BooksDownloader implements Callback<List<Book>> {

    public static final String TAG = BooksDownloader.class.getSimpleName();

    @Inject
    DataBaseRepository dBRepository;

    @Inject
    EventBus eventBus;

    @Inject
    EndPointInterface apiService;

    public BooksDownloader() {
        BookListApplication.app().basicComponent().inject(this);
    }

    public void refreshBooks(String categoryId) {
        String dateDesc = "date:desc";
        String urlForHash = ENDPOINT_BOOK + "?category=" + categoryId + "&sort=" + dateDesc;
        String timestamp = String.valueOf(new Date().getTime());
        String hash = Sha256Hash.hash(urlForHash + timestamp + HEADER_API_TOKEN_VALUE);
        Call<List<Book>> call = apiService.getBookOfCategory(categoryId, dateDesc, timestamp, hash);
        call.enqueue(this);
    }

    @Override
    public void onResponse(Call<List<Book>> call, Response<List<Book>> response) {
        List<Book> newBooks = response.body();
        boolean success = dBRepository.copyToRealmOrUpdate(newBooks);

        if (success) {
            Log.d(TAG, "onResponse: Successfully saved " + dBRepository.count() + " categories.");
            eventBus.post(new OnRefreshedBooks());
        } else {
            eventBus.post(new OnErrorSavingOnDataBase());
        }
    }

    @Override
    public void onFailure(Call<List<Book>> call, Throwable t) {
        eventBus.post(new OnErrorDownloading(t));
    }
}
