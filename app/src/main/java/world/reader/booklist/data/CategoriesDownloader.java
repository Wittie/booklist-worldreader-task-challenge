package world.reader.booklist.data;

import android.util.Log;

import org.greenrobot.eventbus.EventBus;

import java.util.Date;
import java.util.List;

import javax.inject.Inject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import world.reader.booklist.BookListApplication;
import world.reader.booklist.data.events.OnErrorDownloading;
import world.reader.booklist.data.events.OnErrorSavingOnDataBase;
import world.reader.booklist.data.events.OnRefreshedCategories;
import world.reader.booklist.model.Category;
import world.reader.booklist.view.DataBaseRepository;

import static world.reader.booklist.data.EndPointInterface.ENDPOINT_CATEGORIES;
import static world.reader.booklist.data.EndPointInterface.HEADER_API_TOKEN_VALUE;

/**
 * Created by plopez on 23/03/17.
 */

public class CategoriesDownloader implements Callback<List<Category>> {

    public static final String TAG = CategoriesDownloader.class.getSimpleName();

    @Inject
    DataBaseRepository dBRepository;

    @Inject
    EventBus eventBus;

    @Inject
    EndPointInterface apiService;

    public CategoriesDownloader() {
        BookListApplication.app().basicComponent().inject(this);
    }

    public void refreshCategories() {
        String timestamp = String.valueOf(new Date().getTime());
        String hash = Sha256Hash.hash(ENDPOINT_CATEGORIES + timestamp + HEADER_API_TOKEN_VALUE);
        Call<List<Category>> call = apiService.getCategories(timestamp, hash);
        call.enqueue(this);
    }

    @Override
    public void onResponse(Call<List<Category>> call, Response<List<Category>> response) {
        List<Category> newCategories = response.body();
        dBRepository.deleteAllCategories();
        boolean success = dBRepository.copyToRealm(newCategories);
        if (success) {
            Log.d(TAG, "onResponse: Successfully saved " + dBRepository.count() + " categories.");
            eventBus.post(new OnRefreshedCategories());
        } else {
            eventBus.post(new OnErrorSavingOnDataBase());
        }
    }

    @Override
    public void onFailure(Call<List<Category>> call, Throwable t) {
        eventBus.post(new OnErrorDownloading(t));
    }
}
