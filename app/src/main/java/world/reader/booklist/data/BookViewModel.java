package world.reader.booklist.data;

import android.content.Context;
import android.support.annotation.NonNull;
import android.util.Log;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.model.GlideUrl;
import com.bumptech.glide.load.model.LazyHeaders;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;

import java.util.Date;

import javax.inject.Inject;

import world.reader.booklist.BookListApplication;
import world.reader.booklist.R;
import world.reader.booklist.model.RealmBook;

import static world.reader.booklist.data.EndPointInterface.HEADER_ACCEPT_LANGUAGE_KEY;
import static world.reader.booklist.data.EndPointInterface.HEADER_ACCEPT_LANGUAGE_VALUE;
import static world.reader.booklist.data.EndPointInterface.HEADER_API_TOKEN_VALUE;
import static world.reader.booklist.data.EndPointInterface.HEADER_CLIENT_KEY;
import static world.reader.booklist.data.EndPointInterface.HEADER_CLIENT_VALUE;
import static world.reader.booklist.data.EndPointInterface.HEADER_CONTENT_TYPE_KEY;
import static world.reader.booklist.data.EndPointInterface.HEADER_CONTENT_TYPE_VALUE;
import static world.reader.booklist.data.EndPointInterface.HEADER_HASH_KEY;
import static world.reader.booklist.data.EndPointInterface.HEADER_TIMESTAMP_KEY;

/**
 * Created by plopez on 24/03/17.
 */

public class BookViewModel implements RequestListener<GlideUrl, GlideDrawable> {

    public static final String TAG = BookViewModel.class.getSimpleName();

    @Inject
    Context context;

    private RealmBook book;

    public BookViewModel() {
        BookListApplication.app().basicComponent().inject(this);
    }

    public void setBook(RealmBook book) {
        this.book = book;
    }

    public String title() {
        return book.getTitle();
    }

    public String author() {
        return book.getAuthor();
    }

    public void loadIconInto(ImageView imageView) {
        Glide.with(context)
                .load(getGlideUrl(book.getCover()))
                .placeholder(R.drawable.ic_icon_placeholder_gray_24dp)
                .error(R.drawable.ic_icon_error_gray_24dp)
                .listener(this)
                .into(imageView);
    }

    @NonNull
    private GlideUrl getGlideUrl(String iconUrl) {
        final String url = EndPointInterface.SERVER + iconUrl;
        final String timestamp = String.valueOf(new Date().getTime());
        final String hash = Sha256Hash.hash(iconUrl + timestamp + HEADER_API_TOKEN_VALUE);
        return new GlideUrl(url, new LazyHeaders.Builder()
                .addHeader(HEADER_CONTENT_TYPE_KEY, HEADER_CONTENT_TYPE_VALUE)
                .addHeader(HEADER_ACCEPT_LANGUAGE_KEY, HEADER_ACCEPT_LANGUAGE_VALUE)
                .addHeader(HEADER_CLIENT_KEY, HEADER_CLIENT_VALUE)
                .addHeader(HEADER_TIMESTAMP_KEY, timestamp)
                .addHeader(HEADER_HASH_KEY, hash)
                .build());
    }

    @Override
    public boolean onException(Exception e, GlideUrl model, Target<GlideDrawable> target, boolean isFirstResource) {
        Log.d(TAG, "onException: " + e.getLocalizedMessage());
        return false;
    }

    @Override
    public boolean onResourceReady(GlideDrawable resource, GlideUrl model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
        return false;
    }
}

