package world.reader.booklist.data.events;

import world.reader.booklist.model.Category;

/**
 * Created by plopez on 23/03/17.
 */

public class OnClickCategoryEvent {

    private Category category;

    public OnClickCategoryEvent(Category category) {
        this.category = category;
    }

    public Category getCategory() {
        return category;
    }

}
