package world.reader.booklist;

import android.app.Application;

import io.realm.Realm;
import io.realm.RealmConfiguration;

/**
 * Created by plopez on 19/03/17.
 */

public class BookListApplication extends Application {

    private static final long REALM_DATABASE_VERSION = 0;

    private static BookListApplication app;
    private BasicComponent basicComponent;

    @Override
    public void onCreate() {
        super.onCreate();
        Realm.init(this);
        RealmConfiguration realmConfiguration = new RealmConfiguration.Builder()
                .schemaVersion(REALM_DATABASE_VERSION)
                .deleteRealmIfMigrationNeeded()
                .build();

        Realm.compactRealm(realmConfiguration);
        Realm.setDefaultConfiguration(realmConfiguration);

        app = this;
        basicComponent = DaggerBasicComponent.builder()
                .applicationModule(new ApplicationModule(getApplicationContext()))
                .build();
    }

    public static BookListApplication app() {
        return app;
    }

    public BasicComponent basicComponent() {
        return basicComponent;
    }

}
