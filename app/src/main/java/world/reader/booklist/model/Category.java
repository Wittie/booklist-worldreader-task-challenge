package world.reader.booklist.model;

import io.realm.RealmList;
import io.realm.RealmObject;

/**
 * Created by plopez on 19/03/17.
 */

public class Category extends RealmObject {

    public static final String FIELD_ID = "id";
    public static final String FIELD_IS_MAIN_CATEGORY = "isMainCategory";

    String id;
    // List<String> languages; -> List not supported by realm, but not required for this Programming Task
    String title;
    String title_nol10n;
    String description;
    String icon;
    String color;
    boolean isMainCategory;
    RealmList<Category> subcategories;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getTitle_nol10n() {
        return title_nol10n;
    }

    public void setTitle_nol10n(String title_nol10n) {
        this.title_nol10n = title_nol10n;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public boolean isMainCategory() {
        return isMainCategory;
    }

    public void setMainCategory(boolean mainCategory) {
        isMainCategory = mainCategory;
    }

    public RealmList<Category> getSubcategories() {
        return subcategories;
    }

    public void setSubcategories(RealmList<Category> subcategories) {
        this.subcategories = subcategories;
    }

}
