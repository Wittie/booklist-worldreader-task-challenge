package world.reader.booklist.model;

import static world.reader.booklist.model.RealmBook.CATEGORIES_SEPARATOR;

/**
 * Created by plopez on 23/03/17.
 */

public class Book {

    private String author;
    private String description;
    private String language;
    private String published;
    private String title;
    private int version;
    private String tagNames;
    private String content;
    // private LinkedTreeMap<String, String> categoryNames; -> Not Supported by Realm. Not really required for this task
    private String cover;
    private float score;
    private boolean offline;
    private int size;
    private int ratings;
    private String rights;
    private int opens;
    private String publisher;
    private byte[] categories;
    private String id;
    private String countryOfOrigin;

    public RealmBook extractRealmBook() {
        final String categoriesString = parseCategories(this.categories);
        return new RealmBook(categoriesString, title, author, cover);
    }

    private String parseCategories(byte[] categories) {
        StringBuilder builder = new StringBuilder();
        for(byte category : categories) {
            builder.append(CATEGORIES_SEPARATOR).append(category).append(CATEGORIES_SEPARATOR);
        }
        return builder.toString();
    }
}
