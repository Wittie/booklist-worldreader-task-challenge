package world.reader.booklist.model;

import io.realm.RealmObject;

/**
 * Created by plopez on 23/03/17.
 */

public class RealmBook extends RealmObject {

    public static final String FIELD_CATEGORIES = "categories";
    public static final String CATEGORIES_SEPARATOR = "-";

    private String categories;
    private String author;
    private String title;
    private String cover;

    public RealmBook() { }

    public RealmBook(String categories, String title, String author, String cover) {
        this.categories = categories;
        this.title = title;
        this.author = author;
        this.cover = cover;
    }

    public String getCategories() {
        return categories;
    }

    public void setCategories(String categories) {
        this.categories = categories;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getCover() {
        return cover;
    }

    public void setCover(String cover) {
        this.cover = cover;
    }
}
