package world.reader.booklist.view.bookslist;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.Objects;

import javax.inject.Inject;

import world.reader.booklist.BookListApplication;
import world.reader.booklist.R;
import world.reader.booklist.data.BooksDownloader;
import world.reader.booklist.data.events.OnErrorDownloading;
import world.reader.booklist.data.events.OnErrorSavingOnDataBase;
import world.reader.booklist.data.events.OnRefreshedBooks;
import world.reader.booklist.databinding.FragmentBooksListBinding;
import world.reader.booklist.model.Category;
import world.reader.booklist.view.DataBaseRepository;

import static android.content.ContentValues.TAG;
import static android.view.View.GONE;

/**
 * Created by plopez on 23/03/17.
 */

public class BooksListFragment extends Fragment {

    public static final String ARG_CATEGORY_ID = "ARG_CATEGORY_ID";

    @Inject
    DataBaseRepository dBRepository;

    @Inject
    EventBus eventBus;

    Category category;

    private final BooksDownloader booksDownloader = new BooksDownloader();

    private final BooksListRecyclerViewAdapter adapter = new BooksListRecyclerViewAdapter();
    private FragmentBooksListBinding binding;

    public BooksListFragment() {
        BookListApplication.app().basicComponent().inject(this);
        setHasOptionsMenu(true);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_books_list, container, false);

        final LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(getContext(), layoutManager.getOrientation());
        dividerItemDecoration.setDrawable(ContextCompat.getDrawable(getContext(), R.drawable.recyclerview_divider));

        binding.booksListRecyclerView.setAdapter(adapter);
        binding.booksListRecyclerView.setLayoutManager(layoutManager);
        binding.booksListRecyclerView.addItemDecoration(dividerItemDecoration);

        return binding.getRoot();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString(ARG_CATEGORY_ID, category.getId());
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (savedInstanceState != null) {
            setCategoryId(savedInstanceState.getString(ARG_CATEGORY_ID));
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        try {
            setUpToolbarForBookListFragment();
            loadBooksFromDataBase();
        } catch (NullPointerException ex) {
            Log.d(TAG, "onCreateView: Problems getting the Category for the books: " + ex.getLocalizedMessage());
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        eventBus.register(this);
        loadBooksFromDataBase();
        displayEmptyStateIfRequired();
    }

    @Override
    public void onPause() {
        eventBus.unregister(this);
        super.onPause();
    }

    private void setUpToolbarForBookListFragment() throws NullPointerException {
        getActivity().setTitle(getString(R.string.fragment_books_list_toolbar_title));
        final ActionBar supportActionBar = ((AppCompatActivity) getActivity()).getSupportActionBar();
        if (supportActionBar != null) {
            supportActionBar.setSubtitle(category.getTitle());
        }
    }

    public void setCategoryId(String categoryId) throws NullPointerException {
        category = dBRepository.getCategoryById(categoryId);
        Objects.requireNonNull(category);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                getActivity().onBackPressed();
                break;
            case R.id.action_refresh:
                refreshData();
                break;
            default:
                break;
        }

        return true;
    }

    public void loadBooksFromDataBase() {
        adapter.setNewBooks(dBRepository.getBooksForCategory(category.getId()));
    }

    private void displayEmptyStateIfRequired() {
        binding.booksEmptyState.setVisibility(adapter.getItemCount() ==  0 ? View.VISIBLE : GONE);
    }

    protected void refreshData() {
        Toast.makeText(getContext(), R.string.toast_refreshing_books, Toast.LENGTH_SHORT).show();
        booksDownloader.refreshBooks(category.getId());
        setStateLoading();
    }

    private void setStateLoading() {
        binding.booksEmptyState.setVisibility(GONE);
    }

    @Subscribe
    public void onMessageEvent(OnRefreshedBooks event) {
        loadBooksFromDataBase();
        displayEmptyStateIfRequired();
    }

    @Subscribe
    public void onMessageEvent(OnErrorDownloading event) {
        Snackbar.make(binding.coordinatorLayout,
                "Error Downloading: " + event.getThrowable().getLocalizedMessage(),
                Snackbar.LENGTH_INDEFINITE);
    }

    @Subscribe
    public void onMessageEvent(OnErrorSavingOnDataBase event) {
        Snackbar.make(binding.coordinatorLayout,
                "Error Saving On Database",
                Snackbar.LENGTH_INDEFINITE);
    }
}
