package world.reader.booklist.view.categorieslist;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import javax.inject.Inject;

import world.reader.booklist.BookListApplication;
import world.reader.booklist.R;
import world.reader.booklist.data.CategoriesDownloader;
import world.reader.booklist.data.events.OnErrorDownloading;
import world.reader.booklist.data.events.OnErrorSavingOnDataBase;
import world.reader.booklist.data.events.OnRefreshedCategories;
import world.reader.booklist.databinding.FragmentCategoriesListBinding;
import world.reader.booklist.view.DataBaseRepository;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;

/**
 * Created by plopez on 19/03/17.
 */

public class CategoriesListFragment extends Fragment {

    @Inject
    DataBaseRepository dBRepository;

    @Inject
    EventBus eventBus;

    private CategoriesDownloader categoriesDownloader = new CategoriesDownloader();

    CategoryListRecyclerViewAdapter adapter = new CategoryListRecyclerViewAdapter();
    private FragmentCategoriesListBinding binding;

    public CategoriesListFragment() {
        BookListApplication.app().basicComponent().inject(this);
        setHasOptionsMenu(true);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_categories_list, container, false);

        final LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(getContext(), layoutManager.getOrientation());
        dividerItemDecoration.setDrawable(ContextCompat.getDrawable(getContext(), R.drawable.recyclerview_divider));

        binding.categoriesListRecyclerView.setAdapter(adapter);
        binding.categoriesListRecyclerView.setLayoutManager(layoutManager);
        binding.categoriesListRecyclerView.addItemDecoration(dividerItemDecoration);

        return binding.getRoot();
    }

    @Override
    public void onStart() {
        super.onStart();
        setUpToolbarForCategoriesListFragment();
    }

    private void setUpToolbarForCategoriesListFragment() {
        getActivity().setTitle(R.string.fragment_categories_list_toolbar_title);
        final ActionBar supportActionBar = ((AppCompatActivity) getActivity()).getSupportActionBar();
        if (supportActionBar != null) {
            supportActionBar.setSubtitle(null);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        eventBus.register(this);
        loadCategoriesFromDataBase();
        displayEmptyStateIfRequired();
    }

    @Override
    public void onPause() {
        eventBus.unregister(this);
        super.onPause();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                getActivity().onBackPressed();
                break;
            case R.id.action_refresh:
                refreshData();
                break;
            default:
                break;
        }

        return true;
    }

    public void loadCategoriesFromDataBase() {
        adapter.setNewCategories(dBRepository.getAllCategories());
    }

    private void displayEmptyStateIfRequired() {
        binding.categoriesEmptyState.setVisibility(adapter.getItemCount() == 0 ? VISIBLE : GONE);
    }

    protected void refreshData() {
        Toast.makeText(getContext(), R.string.toast_refreshing_categories, Toast.LENGTH_SHORT).show();
        categoriesDownloader.refreshCategories();
        setStateLoading();
    }

    private void setStateLoading() {
        binding.categoriesEmptyState.setVisibility(GONE);
    }

    @Subscribe
    public void onMessageEvent(OnRefreshedCategories event) {
        loadCategoriesFromDataBase();
        displayEmptyStateIfRequired();
    }

    @Subscribe
    public void onMessageEvent(OnErrorDownloading event) {
        Snackbar.make(binding.coordinatorLayout,
                "Error Downloading: " + event.getThrowable().getLocalizedMessage(),
                Snackbar.LENGTH_INDEFINITE);
    }

    @Subscribe
    public void onMessageEvent(OnErrorSavingOnDataBase event) {
        Snackbar.make(binding.coordinatorLayout,
                "Error Saving On Database",
                Snackbar.LENGTH_INDEFINITE);
    }
}
