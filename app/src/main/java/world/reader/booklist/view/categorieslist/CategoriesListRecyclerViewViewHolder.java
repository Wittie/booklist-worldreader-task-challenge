package world.reader.booklist.view.categorieslist;

import android.databinding.ViewDataBinding;
import android.support.v7.widget.RecyclerView;

import world.reader.booklist.BR;
import world.reader.booklist.data.CategoryViewModel;
import world.reader.booklist.databinding.RowCategoriesListBinding;
import world.reader.booklist.model.Category;

/**
 * Created by plopez on 19/03/17.
 */

class CategoriesListRecyclerViewViewHolder extends RecyclerView.ViewHolder {

    private ViewDataBinding itemBinding;
    private CategoryViewModel viewModel;

    public CategoriesListRecyclerViewViewHolder(ViewDataBinding itemBinding) {
        super(itemBinding.getRoot());
        this.itemBinding = itemBinding;
        this.viewModel = new CategoryViewModel();
    }

    public void bind(Category category) {
        viewModel.setCategory(category);
        itemBinding.setVariable(BR.viewModel, viewModel);
        itemBinding.executePendingBindings();

        RowCategoriesListBinding rowCategoriesListBinding = (RowCategoriesListBinding) this.itemBinding;
        viewModel.loadIconInto(rowCategoriesListBinding.categoryRowImageView);
    }

}
