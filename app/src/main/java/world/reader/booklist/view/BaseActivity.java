package world.reader.booklist.view;

import android.databinding.DataBindingUtil;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.AnimRes;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuInflater;

import org.greenrobot.eventbus.EventBus;

import javax.inject.Inject;

import world.reader.booklist.BookListApplication;
import world.reader.booklist.R;
import world.reader.booklist.databinding.ActivityMainBinding;

/**
 * Created by plopez on 23/03/17.
 */

public class BaseActivity extends AppCompatActivity {

    @Inject
    EventBus eventBus;

    ActivityMainBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        BookListApplication.app().basicComponent().inject(this);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main);
        setUpToolbar();
    }

    @Override
    protected void onResume() {
        super.onResume();
        eventBus.register(this);
    }

    @Override
    protected void onPause() {
        eventBus.unregister(this);
        super.onPause();
    }

    private void setUpToolbar() {
        setSupportActionBar(binding.toolbar);
        binding.toolbar.setTitleTextColor(ContextCompat.getColor(this, R.color.toolbarText));
        binding.toolbar.setSubtitleTextColor(ContextCompat.getColor(this, R.color.toolbarText));

        ActionBar supportActionBar = getSupportActionBar();
        if (supportActionBar != null) {
            final Drawable upArrow = ContextCompat.getDrawable(this, R.drawable.ic_arrow_back_white_24dp);
            supportActionBar.setHomeAsUpIndicator(upArrow);
            supportActionBar.setDisplayHomeAsUpEnabled(true);
        }
    }

    protected void showFragment(Fragment fragment,
                                @AnimRes int enter, @AnimRes int exit,
                                @AnimRes int popEnter, @AnimRes int popExit ) {
        getSupportFragmentManager().beginTransaction()
                .setCustomAnimations(enter, exit, popEnter, popExit)
                .replace(R.id.fragmentContainer, fragment, fragment.getClass().getSimpleName())
                .addToBackStack(fragment.getClass().getSimpleName())
                .commit();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.toolbar_menu, menu);
        return true;
    }

    @Override
    public void onBackPressed() {
        if (getSupportFragmentManager().getBackStackEntryCount() > 1) {
            getSupportFragmentManager().popBackStack();
        } else {
            finish();
        }
    }
}
