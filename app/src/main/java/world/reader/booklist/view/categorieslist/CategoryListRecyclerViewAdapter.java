package world.reader.booklist.view.categorieslist;

import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import world.reader.booklist.R;
import world.reader.booklist.model.Category;

/**
 * Created by plopez on 19/03/17.
 */

class CategoryListRecyclerViewAdapter extends RecyclerView.Adapter<CategoriesListRecyclerViewViewHolder> {

    private Category[] categories;

    public void setNewCategories(Category[] categories) {
        this.categories = categories;
        notifyDataSetChanged();
    }

    @Override
    public CategoriesListRecyclerViewViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        ViewDataBinding itemBinding = DataBindingUtil.inflate(layoutInflater, R.layout.row_categories_list, parent, false);
        return new CategoriesListRecyclerViewViewHolder(itemBinding);
    }

    @Override
    public void onBindViewHolder(CategoriesListRecyclerViewViewHolder holder, int position) {
        holder.bind(categories[position]);
    }

    @Override
    public int getItemCount() {
        return categories != null ? categories.length : 0;
    }
}
