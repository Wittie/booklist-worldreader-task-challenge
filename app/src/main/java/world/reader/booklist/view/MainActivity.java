package world.reader.booklist.view;

import android.os.Bundle;

import org.greenrobot.eventbus.Subscribe;

import world.reader.booklist.R;
import world.reader.booklist.data.events.OnClickCategoryEvent;
import world.reader.booklist.view.bookslist.BooksListFragment;
import world.reader.booklist.view.categorieslist.CategoriesListFragment;

public class MainActivity extends BaseActivity {

    public static final String TAG = MainActivity.class.getSimpleName();

    CategoriesListFragment categoriesListFragment;
    BooksListFragment booksListFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setUpCategoriesListFragment();
        setUpBooksListFragment();

        if (savedInstanceState == null) {
            showCategoryListFragment();
        }
    }

    private void setUpCategoriesListFragment() {
        categoriesListFragment = (CategoriesListFragment) getSupportFragmentManager().findFragmentByTag(CategoriesListFragment.class.getSimpleName());
        if (categoriesListFragment == null) {
            categoriesListFragment = new CategoriesListFragment();
        }
    }

    private void setUpBooksListFragment() {
        booksListFragment = (BooksListFragment) getSupportFragmentManager().findFragmentByTag(BooksListFragment.class.getSimpleName());
        if (booksListFragment == null) {
            booksListFragment = new BooksListFragment();
        }
    }

    protected void showCategoryListFragment() {
        showFragment(categoriesListFragment, 0, android.R.anim.fade_out, 0, android.R.anim.fade_in);
    }

    protected void showBooksListFragment(String categoryId) {
        booksListFragment.setCategoryId(categoryId);
        showFragment(booksListFragment, R.anim.slide_in_right, R.anim.slide_out_left, R.anim.slide_in_left, R.anim.slide_out_right);
    }

    @Subscribe
    public void onMessageEvent(OnClickCategoryEvent event) {
        showBooksListFragment(event.getCategory().getId());
    }

}
