package world.reader.booklist.view.bookslist;

import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import world.reader.booklist.R;
import world.reader.booklist.model.RealmBook;

/**
 * Created by plopez on 24/03/17.
 */

class BooksListRecyclerViewAdapter  extends RecyclerView.Adapter<BooksListRecyclerViewViewHolder> {

    private RealmBook[] books;

    public void setNewBooks(RealmBook[] books) {
        this.books = books;
        notifyDataSetChanged();
    }

    @Override
    public BooksListRecyclerViewViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        ViewDataBinding itemBinding = DataBindingUtil.inflate(layoutInflater, R.layout.row_books_list, parent, false);
        return new BooksListRecyclerViewViewHolder(itemBinding);
    }

    @Override
    public void onBindViewHolder(BooksListRecyclerViewViewHolder holder, int position) {
        holder.bind(books[position]);
    }

    @Override
    public int getItemCount() {
        return books != null ? books.length : 0;
    }

}
