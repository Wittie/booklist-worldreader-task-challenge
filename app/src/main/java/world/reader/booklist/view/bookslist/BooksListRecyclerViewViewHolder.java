package world.reader.booklist.view.bookslist;

import android.databinding.ViewDataBinding;
import android.support.v7.widget.RecyclerView;

import world.reader.booklist.BR;
import world.reader.booklist.data.BookViewModel;
import world.reader.booklist.databinding.RowBooksListBinding;
import world.reader.booklist.model.RealmBook;

/**
 * Created by plopez on 24/03/17.
 */

class BooksListRecyclerViewViewHolder  extends RecyclerView.ViewHolder {

    private ViewDataBinding itemBinding;
    private BookViewModel viewModel;

    public BooksListRecyclerViewViewHolder(ViewDataBinding itemBinding) {
        super(itemBinding.getRoot());
        this.itemBinding = itemBinding;
        this.viewModel = new BookViewModel();
    }

    public void bind(RealmBook book) {
        viewModel.setBook(book);
        itemBinding.setVariable(BR.viewModel, viewModel);
        itemBinding.executePendingBindings();

        if (book.getCover() != null) {
            RowBooksListBinding rowCategoriesListBinding = (RowBooksListBinding) this.itemBinding;
            viewModel.loadIconInto(rowCategoriesListBinding.bookCoverImageView);
        }
    }

}
