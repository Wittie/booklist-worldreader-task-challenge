package world.reader.booklist.view;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import java.util.ArrayList;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmResults;
import io.realm.internal.IOException;
import world.reader.booklist.model.Book;
import world.reader.booklist.model.Category;
import world.reader.booklist.model.RealmBook;

import static world.reader.booklist.model.RealmBook.CATEGORIES_SEPARATOR;

/**
 * Created by plopez on 19/03/17.
 */

public class DataBaseRepository {

    private Realm realm;

    public DataBaseRepository(Realm realm) {
        this.realm = realm;
    }

    @Nullable
    public Category getCategoryById(String categoryId) {
        return realm.where(Category.class).equalTo(Category.FIELD_ID, categoryId).findFirst();
    }

    @Nullable
    public RealmBook[] getBooksForCategory(String categoryId) {
        String category = CATEGORIES_SEPARATOR + categoryId + CATEGORIES_SEPARATOR;
        RealmResults<RealmBook> all = realm.where(RealmBook.class).contains(RealmBook.FIELD_CATEGORIES, category).findAll();
        return all.toArray(new RealmBook[all.size()]);
    }

    @NonNull
    public Category[] getAllCategories() {
        RealmResults<Category> all = realm.where(Category.class).equalTo(Category.FIELD_IS_MAIN_CATEGORY, true).findAll();
        return all.toArray(new Category[all.size()]);
    }

    public boolean copyToRealm(List<Category> categories) {
        realm.beginTransaction();
        try {
            for (Category category : categories) {
                category.setMainCategory(true);
                realm.copyToRealm(category);
            }
            realm.commitTransaction();
            return true;
        } catch (IOException e) {
            realm.cancelTransaction();
            return false;
        }
    }

    public void deleteAllCategories() {
        realm.beginTransaction();
        realm.delete(Category.class);
        realm.commitTransaction();
    }

    public boolean copyToRealmOrUpdate(List<Book> newBooks) {

        List<RealmBook> realmBooks = extractRealmBooks(newBooks);

        realm.beginTransaction();
        try {
            realm.copyToRealmOrUpdate(realmBooks);
            realm.commitTransaction();
            return true;
        } catch (IOException e) {
            realm.cancelTransaction();
            return false;
        }
    }

    @NonNull
    private List<RealmBook> extractRealmBooks(List<Book> newBooks) {
        List<RealmBook> realmBooks = new ArrayList<>();
        for (Book book : newBooks) {
            realmBooks.add(book.extractRealmBook());
        }
        return realmBooks;
    }

    public long count() {
        return realm.where(Category.class).count();
    }
}
