package world.reader.booklist;

import android.content.Context;

import com.squareup.okhttp.OkHttpClient;

import org.greenrobot.eventbus.EventBus;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import io.realm.Realm;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import world.reader.booklist.data.EndPointInterface;
import world.reader.booklist.view.DataBaseRepository;

/**
 * Created by plopez on 19/03/17.
 */

@Module
public class ApplicationModule {

    private Context context;

    public ApplicationModule(Context context) {
        this.context = context;
    }

    @Provides
    @Singleton
    Context providesContext() {
        return context;
    }

    @Provides
    @Singleton
    OkHttpClient providesOkHttpClient() {
        return new OkHttpClient();
    }

    @Provides
    @Singleton
    Realm providesRealmDefaultInstance() {
        return Realm.getDefaultInstance();
    }

    @Provides
    @Singleton
    DataBaseRepository providesDataBaseRepository() {
        return new DataBaseRepository(providesRealmDefaultInstance());
    }

    @Provides
    @Singleton
    EventBus providesEventBus() {
        return EventBus.getDefault();
    }

    @Provides
    @Singleton
    EndPointInterface providesEndPointInterface() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(EndPointInterface.SERVER)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        return retrofit.create(EndPointInterface.class);
    }
}