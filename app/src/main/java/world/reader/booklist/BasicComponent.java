package world.reader.booklist;

import javax.inject.Singleton;

import dagger.Component;
import world.reader.booklist.data.BookViewModel;
import world.reader.booklist.data.BooksDownloader;
import world.reader.booklist.data.CategoriesDownloader;
import world.reader.booklist.data.CategoryViewModel;
import world.reader.booklist.view.BaseActivity;
import world.reader.booklist.view.bookslist.BooksListFragment;
import world.reader.booklist.view.categorieslist.CategoriesListFragment;

/**
 * Created by plopez on 19/03/17.
 */

@Singleton
@Component(modules = {ApplicationModule.class})
public interface BasicComponent {

    void inject(BaseActivity baseActivity);

    void inject(CategoryViewModel viewModel);
    void inject(BookViewModel viewModel);

    void inject(CategoriesListFragment fragment);
    void inject(BooksListFragment fragment);

    void inject(CategoriesDownloader downloader);
    void inject(BooksDownloader downloader);

}
