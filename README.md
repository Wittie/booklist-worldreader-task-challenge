# README #

### What is this repository for? ###

* Simple Android app that uses Worldreaders's book API.

### Documentation ###

* Code should be self-explanatory for specific functionality. Overall, the app follows the MVVM architecture using Android's Data Binding capatibilities. As a typical master-detail example, it consists of one activity with two fragments, one general with the categories an another for the books listed by a category. 
* It uses Retrofit y OkHttp for the communication with the server.
* Persistence is achieved with Realm
* Dependency injection with Dagger
* Sporadic code simplification with Events of EventBus
* Glide is used for the download of the category and cover icons
* I am not proud of the UI, but I have assumed that the objetives of this task are more related to the architecture than to the beauty of the UI. And making a nice UI takes a lot of time.

I will be happy to discuss anything about this code in particular, and Android in general.